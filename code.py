import string

def transform(word, word_list):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    transformed_word = ""
    for i in range(len(word)):
        for letter in alphabet:
            if letter != word[i]:
                new_word = word[:i] + letter + word[i+1:]
                if new_word in word_list:
                    transformed_word = new_word
    
    return transformed_word

def load_data(word_file):
    return [word.strip().lower() for word in open(word_file)]    

def show(initial_word: str, final_word: str, word_list):
    route = []
    
    while initial_word != final_word:
        initial_word = transform(initial_word, word_list)
        route.append(initial_word)
    return route

word_list = load_data("sowpods.txt")
initial_word = "clay"
final_word = "gold"
transformations = show(initial_word, final_word, word_list)
print(transformations)
